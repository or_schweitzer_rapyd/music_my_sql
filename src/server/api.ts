// require('dotenv').config();
import express,{Application} from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";
import { connect_mongodb } from "./db/mongoose.connection.js";
import user_router from "./modules/user/user.controller.js";
import playlist_router from "./modules/playlist/playlist.controller.js";
import song_router from "./modules/song/song.controller.js";
import artist_router from "./modules/artist/artist.controller.js";
import logIn_router from "./logIn.controller.js";
import cookieParser from "cookie-parser";
import { connectToMySQLDb } from "./db/mysql.connection.js";
import schedulerToStart from "./scheduler/scheduler.js";


import {
    errLogger,
    not_found,
} from "./middleware/errors.handler.js";

import {generateRequestID} from "./middleware/idGenerator.js";
import { logger } from "./middleware/logger.js";

const { PORT = 8080, HOST = "localhost",DB_TYPE, DB_URI } = process.env;


class MyApp{

    private app: Application;
    private scheduler;

    
    constructor() { 
        this.app = express();

        this.scheduler = this.initScheduler();
       
        // middlewares
        this.initMiddlewares();
        
        // routes
        this.initRoutes();
 
        //when no routes were matched...
        this.app.use("*", not_found);
        
         // final error handling
         this.app.use(errLogger);

    }

    private initMiddlewares(){
        this.app.use(cors());
        this.app.use(morgan("dev"));
        this.app.use(generateRequestID);
        this.app.use(logger);
        this.app.use(cookieParser());
    }

    private initRoutes(){
        // routing
        this.app.use("/api/users", user_router);
        this.app.use("/api/artists",artist_router);
        this.app.use("/api/playlists",playlist_router);
        this.app.use("/api/songs",song_router);
        this.app.use("/api/manageAccount",logIn_router);
       
    }

    private initScheduler(){
        return schedulerToStart.start();
    }
       
    //start the express api server
     start = async () =>{
         if(DB_TYPE==="mongo"){
            await connect_mongodb(DB_URI as string);   
         }else{
            await connectToMySQLDb();
         }
        await (this.app.listen(Number(PORT), HOST as string));
        log.magenta("api is live on", ` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);
    };
    
}

const myApp = new MyApp();
myApp.start().catch(console.log);
