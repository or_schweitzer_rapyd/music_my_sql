import { Request,Response, NextFunction } from "express";
//export default fn => (req:Request, res: Response, next:NextFunction => fn(req, res, next).catch(next);


type f = (req: Request, res: Response, next: NextFunction) => Promise<any>;
export default function raw(func: f) {
    return async function (req: Request, res: Response, next: NextFunction) {
        func(req, res, next).catch((err) => next(err));
    };
}
