import log from "@ajar/marker";
import { Request,Response, NextFunction } from "express";
import HttpException from "../Exceptions/Http.exception.js";
import UrlNotFoundException from "../Exceptions/UrlNotFound.exception.js";
import fs from "fs/promises";


export const error_handler =  (err: Error, req: Request, res:Response, next:NextFunction) => {
    log.error(err);
    next(err);
};

export const errLogger = (err:HttpException,req:Request, res:Response,next:NextFunction)=>{
    const date = new Date();
    const dateStr = `${date.toDateString()} ${date.toTimeString()}`;
    err.status = err.status || 500;
    fs.writeFile(`${process.cwd()}/myErrorlog.log`,
    `RequestID: ${req.myID} ,    Status Code: ${err.status} ,     on: ${dateStr},    message:${err.message},    stack:${err.stack}
    `,
     {
        flag: "a"
    });
    
    res.status(err.status).json({message: err.message,
                          status : err.status });
};

export const not_found =  (req : Request, res : Response,next:NextFunction) => {
    next(new UrlNotFoundException(req.originalUrl));
    
};


