
import {ArtistID, IService,Song} from "../../GlobalTypes.js";
import artistService from "../artist/artist.service.js";
import songModel from "./song.model.mysql.js";



export default class SongMySqlService implements IService<Song>{
    
    async findByID(ID: string): Promise<Song> {
        const song = (await songModel.findById(ID)) as Song;
        
        return song;
    }

    async findAll(): Promise<Song[]> {
        const songs = await songModel.findAll();
        
       return songs as Song[];
    }

    async getPage(pageNum :number ,pageSize : number) : Promise<Song[]>{
        /* const users = await songModel
        .find()
        .skip(pageNum * pageSize)
        .limit(pageSize);
 */
        return [];
    }

    async save(item: Song): Promise<Song> {
        console.log("saving song");
        const artistID = ((item.artist) as ArtistID);
        
        //check artist exist
        const foundArtist = await (artistService.findByID(item.artist as ArtistID));
        
        console.log({foundArtist});
        if(foundArtist === null){
            throw new Error("artist not exist");

        }
        const savedSong = await songModel.create(item);

        return savedSong as unknown as Song;
    }


    async updateByID(ID:string,song:Song) {
   
         const savedSong = await songModel.findByIdAndUpdate(
            ID,
           song
        );
         return savedSong as unknown as Song;
       
    }


    async deleteByID(ID: string): Promise<Song> {
       const removedSong = await songModel.findByIdAndRemove(ID);
    
       return removedSong as unknown as Song;
      
    }

}

