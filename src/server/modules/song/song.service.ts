import SongMongoService from "./song.service.mongo.js";
import SongMySqlService from "./song.service.mysql.js";
import { IService,Song } from "../../GlobalTypes.js";


const { DB_TYPE } = process.env;
let service : IService<Song>;

const db_type_lower: string = (DB_TYPE as string).toLowerCase();
switch (db_type_lower) {
    case "mongo":
        service =  new SongMongoService();
        break;
    
    case "mysql":
        service =  new SongMySqlService();
        break;
        
    default:
        throw new Error("repository is not defined");
}

export default service;

   
