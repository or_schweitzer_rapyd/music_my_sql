
import {IService,Song,SongID,ArtistID, PlaylistID} from "../../GlobalTypes.js";
import songModel from "./song.model.mongo.js";
import artistService from "../artist/artist.service.js";
import playlistService from "../playlist/playlist.service.js";
import mongoose,{ Document } from "mongoose";
import pkg from "bluebird";

const { Promise } = pkg;
export default class SongMongoService implements IService<Song>{

   
    
    async findByID(ID: string): Promise<Song> {
        
        if(!mongoose.isValidObjectId(ID)){
            return ((null as unknown) as Song);
        }

        const song = await songModel.findById(ID);
        
        return song;
    }

    async findAll(): Promise<Song[]> {

        const songs = await 
        songModel.find().select(`id name artist playlists  
                                           `);

       return songs;
    }

    async getPage(pageNum :number ,pageSize : number) : Promise<Song[]>{
        const songs = await songModel
        .find()
        .skip(pageNum * pageSize)
        .limit(pageSize);

        return songs;
    }

    async save(item: Song): Promise<Song> {

        const artistID = ((item.artist) as ArtistID);
        
        //check artist exist
        const foundArtist = await (artistService.findByID(item.artist as ArtistID));
        
        if(foundArtist === null){
            throw new Error("artist not exist");

        }

        //save song if artist exist
        const savedSong = await (songModel.create(item));
        

        //update artist accordingly
        foundArtist.songs?.push(savedSong.id as SongID);
        (foundArtist as Document).save();
    
        return (savedSong as unknown) as Song;
    }

    
    async updateByID(ID:string,song:Song) {
   
        const savedSong = await songModel.findByIdAndUpdate(
            ID,
           song,
            { new: true, upsert: false }
        );

        return (savedSong as unknown) as Song;
    }


    async deleteByID(ID: string): Promise<Song> {
        console.log("start execute delete service");
        const removedSong = (await (songModel.findByIdAndRemove(ID))) as Song;
         
        //remove song also from artist songs
        console.log("before update artist");
        const foundArtist = await (artistService.findByID(removedSong.artist as ArtistID));
        foundArtist.songs = (foundArtist.songs as SongID[]).filter( songID => songID !== ID);
         await (artistService.updateByID(foundArtist.id as ArtistID,foundArtist));
        
         //remove song from all playlists
       await Promise.each((removedSong.playlists) as PlaylistID[],
        async (playlistID) => {
            //get playlist from db
           const playlistToUpdate = await playlistService.findByID(playlistID);
          
            //remove song from playlist
           playlistToUpdate.songs =  playlistToUpdate.songs?.
                                    filter(songID => songID.toString() !== ID);
            //update playlist in db
            await playlistService.updateByID(playlistID,{"songs" : playlistToUpdate.songs });
        });

        return (removedSong as unknown) as Song;
    }
}

