import mysql from "mysql2/promise";
import { Song } from "../../GlobalTypes.js";
import { ResultSetHeader } from "mysql2";
import {connection as mySqlConnection} from "../../db/mysql.connection.js";
class SongModelMySQL {
    public async findById(ID : string){
        const results = await mySqlConnection.query(
                "SELECT * FROM SONG WHERE id = ?",
                [ID]);
        const matches = results[0] as any[];
        const foundSong = matches[0] || null;   
        
        return foundSong;
    }

    public async findAll(){
        const results = await mySqlConnection.query("SELECT * FROM SONG",[]);
        const allSongs = results[0];

        return allSongs;
    }

     public async create(song: Song){
        song.status_id = 1;
        
        const results = await mySqlConnection.query(
            
            "INSERT INTO SONG SET ?",[song]);
        const idStr = (results[0] as ResultSetHeader).insertId.toString();
        const savedSong = await this.findById(idStr);

        return savedSong;
    } 


    public async findByIdAndRemove(id: string){
        const results = await mySqlConnection.query(
            "DELETE FROM SONG WHERE id = ?",[id]);
        const deletedSong = results[0];
       

        return deletedSong;
    }

    public async findByIdAndUpdate(id:string,song : Song){
        const results = await mySqlConnection.query(
            `UPDATE SONG
            SET ?
            WHERE id = ?;`,[song,id]
        );

        const savedSong = await this.findById(id);

       return savedSong;
    }

}

const model = new SongModelMySQL();
export default model;
