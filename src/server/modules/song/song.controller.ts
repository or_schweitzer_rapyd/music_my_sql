/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
  import raw from "../../middleware/route.async.wrapper.js";
  import express from "express";
  import { Request,Response, NextFunction } from "express";
  import songService from "./song.service.js";
  /*import UserNotFoundException from "../../Exceptions/UserNotFound.exception.js";
   */import mongoose from "mongoose";
  
  import {
      validateUpdateUser,
      validateCreateUser,
  } from "../../middleware/validation.mw.js";
import { Song } from "../../GlobalTypes.js";
import SongNotFoundException from "../../Exceptions/SongNotFound.exception.js";
  
  const router = express.Router({mergeParams:true});
  
  // parse json req.body on post routes
  router.use(express.json());

  router.use(function (req,res,next) {
    console.log(req.params);
    next();
});
  
  
  async function findByIdElseThrow(ID : string){
      const foundSong = await (songService.findByID(ID));
      
          if(foundSong !== null){
              return foundSong;
          }else{
              throw new SongNotFoundException(ID);
          }
  }
  
  // GET ALL SONGS
  router.get(
      "/",
      raw(async (req:Request, res:Response) => { 
        const songs = await songService.findAll();
        
        res.status(200).json(songs);
      })
  );
  
  // GETS A SINGLE SONG
  router.get(
      "/:id",
      raw(async (req:Request, res:Response) => {
          const song = await findByIdElseThrow(req.params.id);
         
          res.status(200).json(song);
      })
  );

  // UPDATES A SINGLE SONG(excluding artists and playlists)
  router.put(
      "/:id",
      //raw(validateUpdateUser),
      raw(async (req:Request, res:Response) => { 

       //validating
       const foundSong = await findByIdElseThrow(req.params.id);
       
       //excluding songs
       delete (req.body as Song).artist;
       delete (req.body as Song).playlists;
    

        //saving
        const song = await songService.updateByID(req.params.id,req.body);
        
        res.status(200).json(song);
      })
  );
  
  // DELETES A SONG
  router.delete(
      "/:id",
      raw(async (req:Request, res:Response) => {
          await findByIdElseThrow(req.params.id);
          console.log("checking song service");
          const song = await songService.deleteByID(req.params.id);
  
          res.status(200).json(song);
      })
  );
  
  router.get(
      "/pagination/:pageNum/:pageSize",
      raw(async (req:Request, res:Response) => {
        const pageNum =  Number(req.params.pageNum);
        const pageSize = Number(req.params.pageSize);
         const songs = await songService.getPage(pageNum,pageSize);
          
         res.json(songs);
      })
  );
  
  export default router;
  