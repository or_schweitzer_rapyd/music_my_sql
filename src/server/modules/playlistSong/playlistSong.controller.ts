/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
  import raw from "../../middleware/route.async.wrapper.js";
  import express from "express";
  import { Request,Response, NextFunction } from "express";
  import songService from "../song/song.service.js";
  import playlistService from "../playlist/playlist.service.js";
  import { Playlist, PlaylistID, Song, SongID } from "../../GlobalTypes.js";
  import mongoose from "mongoose";
  
  import {
      validateUpdateUser,
      validateCreateUser,
  } from "../../middleware/validation.mw.js";
import playlistSongService from "./playlistSongService.js";
  
  const router = express.Router({mergeParams:true});
  
  // parse json req.body on post routes
  router.use(express.json());

 //search for given artist in db
 // if not found throw an error
async function findByIdElseThrow(ID : string){
   
    const foundPlaylist = await (playlistService.findByID(ID));
    
        if(foundPlaylist !== null){
            return foundPlaylist;
        }else{
            console.log("throw here");
            throw new Error(ID);
        }
}

//======================Routing Functions========================  

//This router path is :  /playlists/:playlistID/songs
  


// Add SONG FOR A GIVEN PLAYLIST   
  router.post("/",
      //raw(validateCreatePlaylist,
      raw(async (req: Request, res:Response) => {
        const {playlistID} = req.params;
        const {id : songID } = req.body;

        //searching for playlist with identical id
        const givenPlaylist = (await findByIdElseThrow(req.params.playlistID)) as Playlist;
       
         //searching for song with identical song id
         const foundSong = (await songService.findByID(req.body.id)) as Song;
         if(foundSong === null){
             throw new Error("song not exist");
         }
        const updatedPlaylist = await playlistSongService.addSongToPlaylist(playlistID,songID);
        
        
        res.status(200).json(updatedPlaylist);
      })
  );


  // DELETE SONG FROM A GIVEN PLAYLIST 
  router.delete("/:songID",
      //raw(validateCreatePlaylist,
      raw(async (req: Request, res:Response) => {
        const {playlistID,songID} = req.params;
        
        //searching for playlist with identical id
        const givenPlaylist = (await findByIdElseThrow(playlistID)) as Playlist;
       
         //searching for song with identical song id
         const foundSong = (await songService.findByID(songID)) as Song;
         if(foundSong === null){
             throw new Error("song not exist");
         }

         const updatedPlaylist = await playlistSongService.removeSongFromPlaylist(playlistID,songID);
        
        
        res.status(200).json(updatedPlaylist);
      })
  );
  

  
  
  export default router;
  