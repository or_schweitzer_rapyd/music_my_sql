import { IPlaylistSongService } from "./playlistSongService.js";
import songService from "../song/song.service.js";
import playlistService from "../playlist/playlist.service.js";
import { Playlist, PlaylistID, SongID } from "../../GlobalTypes.js";


export default class PlaylistSongMongoService implements IPlaylistSongService{
    
    async removeSongFromPlaylist(playlistID: PlaylistID, songID: SongID): Promise<Playlist> {

        const foundSong  = await songService.findByID(songID);
        const givenPlaylist  = await playlistService.findByID(playlistID);

         //delete given playlist from song's playlist
         foundSong.playlists = foundSong.playlists?.filter(
            (p:PlaylistID) =>  p.toString() !==  playlistID);

        //delete song from given playlist
        givenPlaylist.songs = givenPlaylist.songs?.filter(
            (s:SongID) =>  s.toString() !==  songID);


        //update song
        await songService.updateByID(foundSong.id as SongID,
                                     {"playlists" : foundSong.playlists}
                                     );


        //update playlist 
        const updatedPlaylist = await playlistService.updateByID(givenPlaylist.id as PlaylistID,
            {"songs" : givenPlaylist.songs});
        

       return updatedPlaylist;
    }


   async  addSongToPlaylist(playlistID: PlaylistID, songID: SongID): Promise<Playlist> {
       
       //update song
       const foundSong  = await songService.findByID(songID);
       foundSong.playlists?.push(playlistID);
       await songService.updateByID(foundSong.id as SongID,
                                    {"playlists" : foundSong.playlists}
                                    );
       //update playlist
       const givenPlaylist = await playlistService.findByID(playlistID);
       givenPlaylist.songs?.push(foundSong.id as SongID);
       const updatedPlaylist = await playlistService.updateByID(givenPlaylist.id as PlaylistID,
           {"songs" : givenPlaylist.songs});


        return updatedPlaylist;
    }
    
}