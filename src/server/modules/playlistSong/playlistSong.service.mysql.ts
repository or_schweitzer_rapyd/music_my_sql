import { Playlist } from "../../GlobalTypes.js";
import playlistSongModel from "./playlistSong.model.mysql.js";
import playlistService from "../playlist/playlist.service.js";
import { IPlaylistSongService } from "./playlistSongService.js";

export default class PlaylistSongMySQLService implements IPlaylistSongService{
    
    async removeSongFromPlaylist(playlistID: string, songID: string): Promise<Playlist> {
       const playlistSongs = await playlistSongModel.delete(playlistID,songID);
       const updatedPlaylist = await playlistService.findByID(playlistID);
       updatedPlaylist.songs = playlistSongs;

       return updatedPlaylist;
       
    }

   async addSongToPlaylist(playlistID: string, songID: string): Promise<Playlist> {
       const playlistSongs = await playlistSongModel.create(playlistID,songID);
       const updatedPlaylist = await playlistService.findByID(playlistID);
       updatedPlaylist.songs = playlistSongs;
       
       return updatedPlaylist;
       
    }
}