import mysql from "mysql2/promise";
import { Playlist, PlaylistID, Song, SongID } from "../../GlobalTypes.js";
import { ResultSetHeader } from "mysql2";
import playlistModel from "../playlist/playlist.model.mongo.js";
import { connection as mySqlConnection } from "../../db/mysql.connection.js";
const { HOST, SQL_PORT, DB_NAME, DB_USER_NAME,DB_USER_PASSWORD } = process.env;

class PlaylistSongModelMySQL {
   
    public async findAllPlaylistSongs(playlistID:PlaylistID){
        const results = await mySqlConnection.query(
            "SELECT * FROM PLAYLIST_SONG WHERE (playlist_id = ?)",
            [playlistID]);
        const allPlaylistsSongs = results[0];

        return allPlaylistsSongs as unknown as SongID[];
    }

     public async create(playlist_id : PlaylistID,song_id:SongID){
        const results = await mySqlConnection.query(
            "INSERT INTO PLAYLIST_SONG set ?",[{playlist_id,song_id}]);
        const allPlaylistsSongs = await this.findAllPlaylistSongs(playlist_id);

        return allPlaylistsSongs as unknown as SongID[];
    } 


    public async delete(playlist_id: PlaylistID,song_id:SongID){
        const results = await mySqlConnection.query(
            "DELETE FROM PLAYLIST_SONG WHERE (playlist_id = ? AND song_id = ?)",[playlist_id,song_id]);
        
        const allPlaylistsSongs = await this.findAllPlaylistSongs(playlist_id);
       

        return allPlaylistsSongs as unknown as SongID[];
    }

    
}

const model = new PlaylistSongModelMySQL();
export default model;
