import { Playlist, PlaylistID, SongID } from "../../GlobalTypes.js";
import PlaylistSongMongoService from "./playlistSong.service.mongo.js";
import PlaylistSongSQLService from "./playlistSong.service.mysql.js";

export interface IPlaylistSongService{
     removeSongFromPlaylist(playlistID: PlaylistID,songID:SongID) : Promise<Playlist>;
     addSongToPlaylist(playlistID: PlaylistID,songID:SongID) : Promise<Playlist>;
}

const {DB_TYPE} = process.env;
let service : IPlaylistSongService;
const db_type_lower: string = (DB_TYPE as string).toLowerCase();
switch (db_type_lower) {
    case "mongo":
        service =  new PlaylistSongMongoService();
        break;

   case "mysql":
        service =  new PlaylistSongSQLService();
        break;
    
    default:
        throw new Error("Service is not defined");
    
}

export default service;

   

