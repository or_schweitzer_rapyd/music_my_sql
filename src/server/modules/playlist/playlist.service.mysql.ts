
import {IService,Playlist} from "../../GlobalTypes.js";
import playlistModel from "./playlist.model.mysql.js";



export default class PlaylistMySqlService implements IService<Playlist>{
    
    async findByID(ID: string): Promise<Playlist> {
        const playlist = (await playlistModel.findById(ID)) as Playlist;
        
        return playlist;
    }

    async findAll(): Promise<Playlist[]> {
        const playlists = await playlistModel.findAll();
        
       return playlists as Playlist[];
    }

    async getPage(pageNum :number ,pageSize : number) : Promise<Playlist[]>{
        /* const users = await playlistModel
        .find()
        .skip(pageNum * pageSize)
        .limit(pageSize);
 */
        return [];
    }

    async save(item: Playlist): Promise<Playlist> {
        const savedPlaylist = await playlistModel.create(item);

        return savedPlaylist as unknown as Playlist;
    }


    async updateByID(ID:string,playlist:Playlist) {
   
         const savedPlaylist = await playlistModel.findByIdAndUpdate(
            ID,
           playlist
        );
         return savedPlaylist as unknown as Playlist;
       
    }


    async deleteByID(ID: string): Promise<Playlist> {
       const removedPlaylist = await playlistModel.findByIdAndRemove(ID);
    
       return removedPlaylist as unknown as Playlist;
      
    }

}

