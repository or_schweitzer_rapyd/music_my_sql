import mysql from "mysql2/promise";
import { Playlist } from "../../GlobalTypes.js";
import { ResultSetHeader } from "mysql2";
import { connection as mySqlConnection } from "../../db/mysql.connection.js";

const { HOST, SQL_PORT, DB_NAME, DB_USER_NAME,DB_USER_PASSWORD } = process.env;

class PlaylistModelMySQL {
    
    public async findById(ID : string){
        const results = await mySqlConnection.query(
                "SELECT * FROM PLAYLIST WHERE id = ?",
                [ID]);
        const matches = results[0] as any[];
        const foundPlaylist = matches[0] === undefined? null : matches[0];   
        
        return foundPlaylist;
    }

    public async findAll(){
        const results = await mySqlConnection.query("SELECT * FROM PLAYLIST",[]);
        const allPlaylists = results[0];

        return allPlaylists;
    }

     public async create(playlist: Playlist){
        const results = await mySqlConnection.query(
            "INSERT INTO PLAYLIST SET ?",[playlist]);
        const idStr = (results[0] as ResultSetHeader).insertId.toString();
        const savedPlaylist = await this.findById(idStr);

        return savedPlaylist;
    } 


    public async findByIdAndRemove(id: string){
        const results = await mySqlConnection.query(
            "DELETE FROM PLAYLIST WHERE id = ?",[id]);
        const deletedPlaylist = results[0];
       

        return deletedPlaylist;
    }

    public async findByIdAndUpdate(id:string,playlist : Playlist){
        const results = await mySqlConnection.query(
            `UPDATE Playlist
            SET ?
            WHERE id = ?;`,[playlist,id]
        );
        const savedPlaylist = await this.findById(id);

        return savedPlaylist;
    }

}

const model = new PlaylistModelMySQL();
export default model;
