import PlaylistMongoService from "./playlist.service.mongo.js";
import PlaylistSQLService from "./playlist.service.mysql.js";
import { IService,Playlist } from "../../GlobalTypes.js";


const { DB_TYPE } = process.env;
let service : IService<Playlist>;

const db_type_lower: string = (DB_TYPE as string).toLowerCase();
switch (db_type_lower) {
    case "mongo":
        service =  new PlaylistMongoService();
        break;

    case "mysql":
        service =  new PlaylistSQLService();
        break;
    
    default:
        throw new Error("Service is not defined");
    
}

export default service;

   
