/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
  import raw from "../../middleware/route.async.wrapper.js";
  import express from "express";
  import { Request,Response, NextFunction } from "express";
  import playlistService from "./playlist.service.js";
  import playlistSong_router from "../playlistSong/playlistSong.controller.js";
  import { checkToken } from "../../middleware/user.auth.js";
  import mongoose from "mongoose";
  
  import {
      validateUpdateUser,
      validateCreateUser,
  } from "../../middleware/validation.mw.js";
import PlaylistNotFoundException from "../../Exceptions/PlaylistNotFound.exception.js";
  
  const router = express.Router();
  router.use("/:playlistID/songs", playlistSong_router);
  
  
  // parse json req.body on post routes
  router.use(express.json());
  
  
  async function findByIdElseThrow(ID : string){
      
      const foundplaylist = await (playlistService.findByID(ID));
      
          if(foundplaylist !== null){
              return foundplaylist;
          }else{
              throw new PlaylistNotFoundException(ID);
          }
  }
  
  
  
  // CREATES A NEW PLAYLIST
  router.post(
      "/",//raw(checkToken),
      //raw(validateCreatePlaylist,
      raw(async (req: Request, res:Response) => {
          const playlist = await playlistService.save(req.body);
          res.status(200).json(playlist);
      })
  );
  
  // GET ALL PLAYLISTS
  router.get(
      "/",
      raw(async (req:Request, res:Response) => {
          const playlists = await 
          playlistService.findAll();
          res.status(200).json(playlists);
      })
  );
  
  // GETS A SINGLE PLAYLIST
  router.get(
      "/:id",
      raw(async (req:Request, res:Response) => {
          const playlist = await findByIdElseThrow(req.params.id);
         
          res.status(200).json(playlist);
      })
  );
  // UPDATES A SINGLE PLAYLIST
  router.put(
      "/:id",
      //raw(validateUpdateUser),
      raw(async (req:Request, res:Response) => { 
        await findByIdElseThrow(req.params.id);
        const playlist = await playlistService.updateByID(req.params.id,req.body);
        
        res.status(200).json(playlist);
      })
  );
  
  // DELETES A PLAYLIST
  router.delete(
      "/:id",
      raw(async (req:Request, res:Response) => {
          await findByIdElseThrow(req.params.id);
          const playlist = await playlistService.deleteByID(req.params.id);
  
          res.status(200).json(playlist);
      })
  );
  
  //PAGINATION
  router.get(
      "/pagination/:pageNum/:pageSize",
      raw(async (req:Request, res:Response) => {
        const pageNum =  Number(req.params.pageNum);
        const pageSize = Number(req.params.pageSize);
         const playlists = await playlistService.getPage(pageNum,pageSize);
          
         res.json(playlists);
      })
  );
  
  export default router;
  