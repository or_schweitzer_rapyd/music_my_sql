
import {IService,Playlist, SongID} from "../../GlobalTypes.js";
import playlistModel from "./playlist.model.mongo.js";
import songService from "../song/song.service.js";
import pkg from "bluebird";
import mongoose from "mongoose";

const { Promise } = pkg;
export default class playlistMongoService implements IService<Playlist>{

   
    
    async findByID(ID: string): Promise<Playlist> {
        
        if(!mongoose.isValidObjectId(ID)){
            return ((null as unknown) as Playlist);
        }

        const playlist = await playlistModel.findById(ID);
        
        return playlist;
    }

    async findAll(): Promise<Playlist[]> {

        const playlists = await 
        playlistModel.find().select(`id 
                                          name 
                                          songs 
                                           `);

       return playlists;
    }

    async getPage(pageNum :number ,pageSize : number) : Promise<Playlist[]>{
        const playlists = await playlistModel
        .find()
        .skip(pageNum * pageSize)
        .limit(pageSize);

        return playlists;
    }

    async save(item: Playlist): Promise<Playlist> {
        const savedPlaylist = await playlistModel.create(item);

        return (savedPlaylist as unknown) as Playlist;
    }


    async updateByID(ID:string,playlist:Playlist) {
   
        const savedPlaylist = await playlistModel.findByIdAndUpdate(
            ID,
           playlist,
            { new: true, upsert: false }
        );

        return (savedPlaylist as unknown) as Playlist;
    }

    


    async deleteByID(ID: string): Promise<Playlist> {
        const removedPlaylist = await playlistModel.findByIdAndRemove(ID);


        //remove all refernces to playlist
       await Promise.each((removedPlaylist.songs) as SongID[],
       async (songID) => {
           //get song from db
          const songToUpdate = await songService.findByID(songID);
         
           //remove song from playlist
           songToUpdate.playlists =  songToUpdate.playlists?.
                                   filter((songID :SongID)  => songID.toString() !== ID);
           //update playlist in db
           await songService.updateByID(songID,{"playlists" : songToUpdate.playlists });
       });



        return (removedPlaylist as unknown) as Playlist;
    }
}

