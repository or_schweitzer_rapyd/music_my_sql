
import {IService,Artist,ArtistID,Song,SongID} from "../../GlobalTypes.js";
import mongoose from "mongoose";
import artistModel from "./artist.model.mongo.js";
import songService from "../song/song.service.js";
import pkg from "bluebird";

const { Promise } = pkg;
export default class ArtistMongoService implements IService<Artist>{

   
    
    async findByID(ID: string): Promise<Artist> {
        if(!mongoose.isValidObjectId(ID)){
            return ((null as unknown) as Artist);
        }
        const artist = await artistModel.findById(ID).populate("songs");
        
        return artist;
    }

    async findAll(): Promise<Artist[]> {

        const artists = await 
        artistModel.find().select(`id
                                          name 
                                          songs 
                                           `);

       return artists;
    }

    async getPage(pageNum :number ,pageSize : number) : Promise<Artist[]>{
        const artists = await artistModel
        .find()
        .skip(pageNum * pageSize)
        .limit(pageSize);

        return artists;
    }

    
    //assume artist is given without songs
    async save(item: Artist): Promise<Artist> {
        item.songs = [];
        const savedArtist = await artistModel.create(item);

        return (savedArtist as unknown) as Artist;
    }


    
    async updateByID(ID:string,artist:Artist) {
   

        const savedArtist = await artistModel.findByIdAndUpdate(
            ID,
           artist,
            { new: true, upsert: false }
        );

       
        return (savedArtist as unknown) as Artist;
    }

    
    async deleteByID(ID: string): Promise<Artist> {
        const artistToRemove = (await artistModel.findById(ID)) as Artist;
        
      await Promise.each((artistToRemove.songs) as SongID[],
        async (songID: SongID) =>{
                await songService.deleteByID(songID);
       });

         await artistModel.findByIdAndDelete(ID);

        return (artistToRemove as unknown) as Artist;
    }
    
    
}

