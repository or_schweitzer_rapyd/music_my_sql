import mysql from "mysql2/promise";
import { Artist } from "../../GlobalTypes.js";
import { ResultSetHeader } from "mysql2";
import { connection as mySqlConnection } from "../../db/mysql.connection.js";
const { HOST, SQL_PORT, DB_NAME, DB_USER_NAME,DB_USER_PASSWORD } = process.env;

class AritstModelMySQL {
   
    public async findById(ID : string){
        const results = await mySqlConnection.query(
                "SELECT * FROM ARTIST WHERE id = ?",
                [ID]);
        const matches = results[0] as any[];
        const foundArtist = matches[0] || null;
        
        return foundArtist;
    }

    public async findAll(){
        const results = await mySqlConnection.query("SELECT * FROM ARTIST",[]);
        const allArtists = results[0];

        return allArtists;
    }

     public async create(artist: Artist){
        const results = await mySqlConnection.query(
            "INSERT INTO ARTIST SET ?",[artist]);
        const idStr = (results[0] as ResultSetHeader).insertId.toString();
        const savedArtist = await this.findById(idStr);

        return savedArtist;
    } 


    public async findByIdAndRemove(id: string){
        const results = await mySqlConnection.query(
            "DELETE FROM ARTIST WHERE id = ?",[id]);
        const deletedArtist = results[0];
       

        return deletedArtist;
    }

    public async findByIdAndUpdate(id:string,artist : Artist){
        const results = await mySqlConnection.query(
            `UPDATE Artist
            SET ?
            WHERE id = ?;`,[artist,id]
        );

        const savedArtist = await this.findById(id);

       return savedArtist;
    }

}

const model = new AritstModelMySQL();
export default model;
