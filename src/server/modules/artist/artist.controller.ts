/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
  import raw from "../../middleware/route.async.wrapper.js";
  import express from "express";
  import { Request,Response, NextFunction } from "express";
  import artistService from "./artist.service.js";
  import { Artist } from "../../GlobalTypes.js"; 
  import artistSong_router from "./artistSong.controller.js";
  /*import UserNotFoundException from "../../Exceptions/UserNotFound.exception.js";
   */import mongoose from "mongoose";
  
  import {
      validateUpdateUser,
      validateCreateUser,
  } from "../../middleware/validation.mw.js";
import ArtistNotFoundException from "../../Exceptions/ArtistNotFound.exception.js";
  
  const router = express.Router({mergeParams:true});
  
  // parse json req.body on post routes
  router.use(express.json());
  router.use("/:artistID/songs", artistSong_router);
  
  async function findByIdElseThrow(ID : string){
      const foundArtist = await (artistService.findByID(ID));
      
          if(foundArtist !== null){
              return foundArtist;
          }else{
              throw new ArtistNotFoundException(ID);
          }
  }
  
  
  
  // CREATES A NEW ARTIST WITH NO SONGS
  router.post(
      "/",
      //raw(validateCreateartist,
      raw(async (req: Request, res:Response) => {
          const artist = await artistService.save(req.body);
          res.status(200).json(artist);
      })
  );
  
  // GET ALL ARTISTS
  router.get(
      "/",
      raw(async (req:Request, res:Response) => {
          const artists = await artistService.findAll();
          res.status(200).json(artists);
      })
  );
  
  // GETS A SINGLE ARTIST
  router.get(
      "/:id",
      raw(async (req:Request, res:Response) => {
          const artist = await findByIdElseThrow(req.params.id);
         
          res.status(200).json(artist);
      })
  );


  // UPDATES A SINGLE ARTIST (exclude Songs)
  router.put(
      "/:id",
      //raw(validateUpdateUser),
      raw(async (req:Request, res:Response) => { 
        await findByIdElseThrow(req.params.id);
        
        //excluding songs
        delete (req.body as Artist).songs;

        const artist = await artistService.updateByID(req.params.id,req.body);
        
        res.status(200).json(artist);
      })
  );
  
  // DELETE AN ARTIST
  router.delete(
      "/:id",
      raw(async (req:Request, res:Response) => {
          await findByIdElseThrow(req.params.id);
          const artist = await artistService.deleteByID(req.params.id);
  
          res.status(200).json(artist);
      })
  );
  
  //PAGINATION
  router.get(
      "/pagination/:pageNum/:pageSize",
      raw(async (req:Request, res:Response) => {
        const pageNum =  Number(req.params.pageNum);
        const pageSize = Number(req.params.pageSize);
         const artists = await artistService.getPage(pageNum,pageSize);
          
         res.json(artists);
      })
  );
  
  export default router;
  