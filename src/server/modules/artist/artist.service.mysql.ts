
import {IService,Artist} from "../../GlobalTypes.js";
import artistModel from "./artist.model.mysql.js";
import {genAccessToken,genRefreshToken} from "../../middleware/user.auth.js";

const {REFRESH_EXPIRATION,APP_SECRET} = process.env;
export default class ArtistMySqlService implements IService<Artist>{
    
    async findByID(ID: string): Promise<Artist> {
        const artist = (await artistModel.findById(ID)) as Artist;
        
        return artist;
    }

    async findAll(): Promise<Artist[]> {
        const artists = await artistModel.findAll();
        
       return artists as Artist[];
    }

    async getPage(pageNum :number ,pageSize : number) : Promise<Artist[]>{
        /* const users = await artistModel
        .find()
        .skip(pageNum * pageSize)
        .limit(pageSize);
 */
        return [];
    }

    async save(item: Artist): Promise<Artist> {
        const savedArtist = await artistModel.create(item);

        return savedArtist as unknown as Artist;
    }


    async updateByID(ID:string,artist:Artist) {
   
         const savedArtist = await artistModel.findByIdAndUpdate(
            ID,
           artist
        );
         return savedArtist as unknown as Artist;
       
    }


    async deleteByID(ID: string): Promise<Artist> {
       const removedArtist = await artistModel.findByIdAndRemove(ID);
    
       return removedArtist as unknown as Artist;
      
    }

}

