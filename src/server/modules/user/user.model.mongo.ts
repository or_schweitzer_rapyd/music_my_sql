import mongoose from "mongoose";
const { Schema, model } = mongoose;

const { PAGE_SIZE } = process.env;


export const UserSchema = new Schema({
    refresh_token: String,
    first_name: String,
    last_name: String,
    email: String,
    password: String,
    phone: String,
    playlists: [{ type: Schema.Types.ObjectId, ref: "playlist" }]
});

export default model("user", UserSchema);
