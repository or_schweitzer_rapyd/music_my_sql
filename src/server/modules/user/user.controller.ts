/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.js";
import express from "express";
import { Request,Response, NextFunction } from "express";
import userService from "./user.service.js";
import UserNotFoundException from "../../Exceptions/UserNotFound.exception.js";
import mongoose from "mongoose";

import {
    validateUpdateUser,
    validateCreateUser,
} from "../../middleware/validation.mw.js";


const router = express.Router();

// parse json req.body on post routes
router.use(express.json());


async function findByIdElseThrow(ID : string){
    console.log("becfore searching");

    /* if(!mongoose.isValidObjectId(ID)){
        throw new UserNotFoundException(ID);
    } */
    const foundUser = await (userService.findByID(ID));
    
        if(foundUser !== null){
            console.log("not null");
            return foundUser;
        }else{
            throw new UserNotFoundException(ID);
        }
}

// CREATES A NEW USER
router.post(
    "/",
  //  raw(validateCreateUser),
    raw(async (req: Request, res:Response) => {
        req.body.refresh_token = "";
        const user = await userService.save(req.body);
       // genAccessToken(user);
        res.status(200).json(user);
    })
);

// GET ALL USERS
router.get(
    "/",
    raw(async (req:Request, res:Response) => {
        const users = await 
        userService.findAll();
        res.status(200).json(users);
    })
);

// GETS A SINGLE USER
router.get(
    "/:id",
    raw(async (req:Request, res:Response) => {
        const user = await findByIdElseThrow(req.params.id);
       
        res.status(200).json(user);
    })
);
// UPDATES A SINGLE USER
router.put(
    "/:id",
    raw(validateUpdateUser),
    raw(async (req:Request, res:Response) => { 
      await findByIdElseThrow(req.params.id);
      const user = await userService.updateByID(req.params.id,req.body);
      
      res.status(200).json(user);
    })
);

// DELETES A USER
router.delete(
    "/:id",
    raw(async (req:Request, res:Response) => {
        await findByIdElseThrow(req.params.id);
        const user = await userService.deleteByID(req.params.id);

        res.status(200).json(user);
    })
);

router.get(
    "/pagination/:pageNum/:pageSize",
    raw(async (req:Request, res:Response) => {
      const pageNum =  Number(req.params.pageNum);
      const pageSize = Number(req.params.pageSize);
       const users = await userService.getPage(pageNum,pageSize);
        
       res.json(users);
    })
);

export default router;
