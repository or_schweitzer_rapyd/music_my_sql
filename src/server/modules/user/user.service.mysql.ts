
import {IUserService, Playlist, User} from "../../GlobalTypes.js";
import userModel from "./user.model.mysql.js";
import bcrypt from "bcrypt";
import {genAccessToken,genRefreshToken} from "../../middleware/user.auth.js";
import playlistService from "../playlist/playlist.service.js";

const {REFRESH_EXPIRATION,APP_SECRET} = process.env;
export default class UserMySqlService implements IUserService{
   
    async findByID(ID: string): Promise<User> {
        const user = (await userModel.findById(ID)) as User;
        
        return user;
    }

    async findAll(): Promise<User[]> {
        const users = await userModel.findAll();
        
       return users as User[];
    }

    async getPage(pageNum :number ,pageSize : number) : Promise<User[]>{
        /* const users = await userModel
        .find()
        .skip(pageNum * pageSize)
        .limit(pageSize);
 */
        return [];
    }

    async save(item: User): Promise<User> {
        item.password = await bcrypt.hash(item.password as string,7);
        const savedUser = await userModel.create(item);

        return savedUser as unknown as User;
    }


    async updateByID(ID:string,user:User) {
   
         const savedUser = await userModel.findByIdAndUpdate(
            ID,
           user
        );
         return savedUser as unknown as User;
       
    }


    async deleteByID(ID: string): Promise<User> {
       const removedUser = await userModel.findByIdAndRemove(ID);
    
       return removedUser as unknown as User;
      
    }

    async getPlaylists(ID: string): Promise<Playlist[]> {
        return await userModel.getPlaylists(ID);
    }

    async findByEmail(email: string): Promise<User> {
        /* console.log(email);
        const foundUser = await userModel.findOne({"email" : email}).exec(); */
        return null as unknown as User;
        
    }
}

