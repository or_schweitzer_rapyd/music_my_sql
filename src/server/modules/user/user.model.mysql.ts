import mysql from "mysql2/promise";
import { Playlist, User } from "../../GlobalTypes.js";
import { ResultSetHeader } from "mysql2";
import { connection as mySqlConnection } from "../../db/mysql.connection.js";

const { HOST, SQL_PORT, DB_NAME, DB_USER_NAME,DB_USER_PASSWORD } = process.env;

class MySQLModel {
    
    
    public async findById(ID : string){
        const results = await mySqlConnection.query(
                "SELECT * FROM USER WHERE id = ?",
                [ID]);
        const matches = results[0] as any[];
        const foundUser = matches[0] || null;

        return foundUser;
    }


    public async findByEmail(email : string){
        const results = await mySqlConnection.query(
            "SELECT * FROM USER WHERE email = ?",
            [email]);
        const matches = results[0] as any[];
        const foundUser = matches[0] || null;
        
        return foundUser;
    }

    public async findAll(){
        const results = await mySqlConnection.query("SELECT * FROM USER",[]);
        const allUsers = results[0];

        return allUsers;
    }

     public async create(user: User){
        const results = await mySqlConnection.query(
            "INSERT INTO USER SET ?",[user]);
        const idStr = (results[0] as ResultSetHeader).insertId.toString();
        const savedUser = await this.findById(idStr);

        return savedUser;
    } 


    public async findByIdAndRemove(id: string){
        const results = await mySqlConnection.query(
            "DELETE FROM USER WHERE id = ?",[id]);
        const deletedUser = results[0];
        console.log(deletedUser);

        return deletedUser;
    }

    public async findByIdAndUpdate(id:string,user : User){
        const results = await mySqlConnection.query(
            `UPDATE USER
            SET ?
            WHERE id = ?;`,[user,id]
        );

        const savedUser = await this.findById(id);

       return savedUser;
    }

    public async getPlaylists(id: string){
        const results = await mySqlConnection.query(
            `SELECT FROM PlAYLIST
             WHERE id = ?`
            ,[id]
        );
        const playlists = results[0] as unknown as Playlist[];

        return playlists;

    }

}

const model = new MySQLModel();
export default model;
