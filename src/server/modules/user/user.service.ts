import UserMongoService from "./user.service.Mongo.js";
import UserMySqlService from "./user.service.mysql.js";
import { IUserService } from "../../GlobalTypes.js";
import {User} from "../../GlobalTypes.js";

const { DB_TYPE } = process.env;
let service : IUserService;

const db_type_lower: string = (DB_TYPE as string).toLowerCase();
switch (db_type_lower) {
    case "mysql":
        service = new UserMySqlService();
        break;
        
    case "mongo":
        service = new UserMongoService();
        break;

    default:
        throw new Error("service is not defined");
}

export default service;

   
