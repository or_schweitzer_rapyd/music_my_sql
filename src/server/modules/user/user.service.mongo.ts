
import {IUserService, Playlist, User} from "../../GlobalTypes.js";
import userModel from "./user.model.mongo.js";
import bcrypt from "bcrypt";
import {genAccessToken,genRefreshToken} from "../../middleware/user.auth.js";

const {REFRESH_EXPIRATION,APP_SECRET} = process.env;
export default class UserMongoService implements IUserService{
    
    async findByID(ID: string): Promise<User> {
        const user = await userModel.findById(ID);
        
        return user;
    }

    async findAll(): Promise<User[]> {

        const users = await 
        userModel.find().select(`-_id 
                                          first_name 
                                          last_name 
                                          email 
                                          phone`);

       return users;
    }

    async getPage(pageNum :number ,pageSize : number) : Promise<User[]>{
        const users = await userModel
        .find()
        .skip(pageNum * pageSize)
        .limit(pageSize);

        return users;
    }

    async save(item: User): Promise<User> {
        item.password = await bcrypt.hash(item.password as string,7);
        const savedUser = await userModel.create(item);

        return (savedUser as unknown) as User;
    }


    async updateByID(ID:string,user:User) {
   
        const savedUser = await userModel.findByIdAndUpdate(
            ID,
           user,
            { new: true, upsert: false }
        );

        return (savedUser as unknown) as User;
    }


    async deleteByID(ID: string): Promise<User> {
        const removedUser = await userModel.findByIdAndRemove(ID);

        return (removedUser as unknown) as User;
    }

    async findByEmail(email: string): Promise<User> {
        console.log(email);
        const foundUser = await userModel.findOne({"email" : email}).exec();

        return foundUser;
    }

    async getPlaylists(ID: string): Promise<Playlist[]> {
        throw new Error("Method not implemented.");
    }
}

