
export interface IService<T> 
{ 
    findByID(ID: string) : Promise<T>;
    findAll(): Promise<T[]>;
    getPage(pageNum : number, pageSize : number): Promise<T[]>
    save(item:T) : Promise<T>;
    deleteByID(ID: string) : Promise<T>;
    updateByID(ID: string,item: T) : Promise<T>;
}


export interface IUserService extends IService<User>{
    findByEmail(email: string) : Promise<User>;
    getPlaylists(ID: string) : Promise<Playlist[]>
}



export type PlaylistID = string;

export interface Playlist{
    id ?: PlaylistID;
    name ?: string;
    songs ?: SongID[];
    user ?: UserID;
}

export type SongID = string;
export interface Song{
    id ?: SongID
    name ?: string;
    length ?: number;
    artist ?: ArtistID;
    playlists ?: PlaylistID[];
    status_id ?: number;
}


export type ArtistID = string;
export interface Artist{
    id ?: ArtistID
    name ?: string;
    songs ?: SongID[];
}


export type UserID = string;
export interface User {
    id ?: UserID,
    refresh_token ?: string,
    first_name ?: string,
    last_name ?: string,
    email ?: string,
    password ?: string,
    phone ?: string
    roles ?: string

}

export enum Roles{
    ADMIN = 1,
    MODERATOR = 2,//regect or accepts changes
    USER = 3,//Basic functionalitis
}

    
    