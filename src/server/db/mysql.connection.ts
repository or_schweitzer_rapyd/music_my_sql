import mysql from "mysql2/promise";
import log from "@ajar/marker";
const { DB_HOST, SQL_PORT, DB_NAME, DB_USER_NAME, DB_USER_PASSWORD } = process.env;

export let connection: mysql.Connection;
export const connectToMySQLDb = async (): Promise<mysql.Connection | void>=> {

    if(connection){
        return connection;
    }
    connection = await mysql.createConnection({
        host: DB_HOST,
        port: Number(SQL_PORT),
        database: DB_NAME,
        user: DB_USER_NAME,
        password: DB_USER_PASSWORD
    });

    await connection.connect();
    log.magenta(" :sparkles:  Connected to MySql DB :sparkles: ");
};










