import log from "@ajar/marker";
import cron from "node-cron";
import fetch from "node-fetch";
import {exec} from "child_process";
import fs from "fs/promises";

export async function sqlDump(){
    const rawData = await fs.readFile("./config.json","utf8");
    const configFile = JSON.parse(rawData.toString());
    const {DB_NAMES} = configFile;
    console.log(configFile);
    (DB_NAMES as string[])
    .forEach(async db =>{
        console.log(`db is ${db}`);
        //generate backup
        const task =  exec(
            `docker exec mysql-db /usr/bin/mysqldump -u root --password=qwerty ${db}`
        );
        const timeStamp = Date.now();

        
        //send to storage server
        console.log("before fetch"); 
        await fetch("http://localhost:8081/storeDB/", {
            method: "post",
            body: task.stdout,
            headers: {"db-Name": `${db}${timeStamp}`}
           }
          
        );
        console.log("after fetch"); 

    });
}