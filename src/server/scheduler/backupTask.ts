import cron from "node-cron";
import {sqlDump} from "../db/mysql.backup.js";

export const sqlDumpTask = cron.schedule("* * * * *",async () =>  {
    try{
      await sqlDump();

    }catch(e){
      console.log("sqldump not working");
    }
    
  }, {
    scheduled: false
  });

  
  
  