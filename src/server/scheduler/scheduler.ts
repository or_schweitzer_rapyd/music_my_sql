import cron from "node-cron";
import { sqlDumpTask } from "./backupTask.js";

class Scheduler {
    private tasks : cron.ScheduledTask[] = [];

    public addTask(task : cron.ScheduledTask){
        this.tasks.push(task);
    }

    public  start(){
        this.tasks.forEach(t => {
            t.start();
            console.log(t);
        });
        return this;
    }
};

const scheduler = new Scheduler();
scheduler.addTask(sqlDumpTask);


export default scheduler;